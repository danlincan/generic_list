#include <string>
#include <vector>

// unit testing
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

// actual implementation
#include "generic_list.h"

struct tracker {
  tracker(int value) : value(value) { ++cnt; }

  tracker() : tracker(0) {}

  tracker(const tracker &oth) {
    ++cnt;
    value = oth.value;
  }

  tracker(tracker &&oth) {
    ++cnt;
    value = oth.value;
  }

  tracker &operator=(const tracker &oth) = default;
  tracker &operator=(tracker &&oth) = default;

  ~tracker() { --cnt; }

  static void reset() { cnt = 0; }

  int value;
  static int cnt;
};

int tracker::cnt = 0;

TEST_CASE("constructor") {
  generic::list<int> l;
  CHECK(l.size() == 0);
  CHECK(l.head_ == nullptr);
  CHECK(l.tail_ == nullptr);
}

TEST_CASE("constructor_initializer_list_int") {
  generic::list<int> l({0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
  REQUIRE(l.size() == 10);
  int i = 0;
  for (auto it = l.head_; it; it = it->next) {
    CHECK(i == it->value);
    ++i;
  }
}

TEST_CASE("constructor_initializer_list_string") {
  generic::list<std::string> l({"foo", "bar"});
  REQUIRE(l.size() == 2);
  CHECK(l.head_->value == "foo");
  CHECK(l.tail_->value == "bar");
}

TEST_CASE("push_back one element") {
  tracker::reset();
  generic::list<tracker> l;
  l.push_back(tracker(1));
  CHECK(l.size() == 1);
  CHECK(tracker::cnt == 1);
  CHECK(l.head_ == l.tail_);
  CHECK(l.head_->value.value == 1);
}

TEST_CASE("push_back multiple elements") {
  tracker::reset();
  generic::list<tracker> l;
  int expected = 10;
  for (int i = 0; i < expected; ++i) {
    l.push_back(tracker(i));
  }
  REQUIRE(l.size() == expected);
  REQUIRE(tracker::cnt ==
          10); // verify num instances in our tracker object also

  auto el = l.head_;
  for (int i = 0; i < expected; ++i) {
    CHECK(el->value.value == i);
    el = el->next;
  }
}

TEST_CASE("pop_back one element") {
  tracker::reset();
  generic::list<tracker> l;
  l.push_back(1);
  REQUIRE(l.size() == 1);
  REQUIRE(tracker::cnt == 1);

  l.pop_back();
  REQUIRE(l.size() == 0);
  REQUIRE(tracker::cnt == 0);
  REQUIRE(l.head_ == nullptr);
  REQUIRE(l.tail_ == nullptr);
}

TEST_CASE("pop_back multiple elements") {
  tracker::reset();
  generic::list<tracker> l;
  int n = 10;
  for (int i = 0; i < n; ++i) {
    l.push_back(tracker(i));
  }
  REQUIRE(l.size() == n);
  REQUIRE(tracker::cnt == n);

  for (int i = n - 1; i > 0; --i) {
    l.pop_back();
    REQUIRE(l.size() == i);
    REQUIRE(l.tail_->value.value == i - 1);
  }
  l.pop_back();
  CHECK(l.size() == 0);
  CHECK(l.head_ == nullptr);
  CHECK(l.tail_ == nullptr);
}

TEST_CASE("reverse_0_elements") {
  generic::list<int> l;
  REQUIRE(l.size() == 0);
  l.reverse();
  CHECK(l.size() == 0);
  CHECK(l.head_ == nullptr);
  CHECK(l.tail_ == nullptr);
}

TEST_CASE("reverse_one_element") {
  tracker::reset();
  generic::list<tracker> l = {100};

  l.reverse();
  REQUIRE(l.size() == 1);
  REQUIRE(tracker::cnt == 1);
  REQUIRE(l.head_ == l.tail_);
  REQUIRE(l.head_->value.value == 100);
}

TEST_CASE("reverse_two_elements") {
  tracker::reset();
  generic::list<tracker> l = {100, 200};

  l.reverse();

  CHECK(l.size() == 2);
  CHECK(tracker::cnt == 2);

  CHECK(l.head_ != l.tail_);
  CHECK(l.head_->value.value == 200);
  CHECK(l.tail_->value.value == 100);

  CHECK(l.head_->next == l.tail_);
  CHECK(l.head_->prev == nullptr);

  CHECK(l.tail_->next == nullptr);
  CHECK(l.tail_->prev == l.head_);
}

TEST_CASE("reverse_n_elements") {
  tracker::reset();
  generic::list<tracker> l;
  int n = 10;
  // [0, n)
  for (int i = 0; i < n; ++i) {
    l.push_back(tracker(i));
  }
  REQUIRE(l.size() == n);
  REQUIRE(tracker::cnt == n);

  l.reverse();

  REQUIRE(l.size() == n);
  REQUIRE(tracker::cnt == n);
  auto it = l.head_;
  // (n, 0]
  for (int i = n - 1; i >= 0; --i) {
    CHECK(i == it->value.value);
    it = it->next;
  }
}

TEST_CASE("filter_int") {
  generic::list<int> l;
  int n = 10;
  for (int i = 0; i < n; ++i) {
    l.push_back(i);
  }
  auto odd_filter = [](const int &x) { return x % 2 == 1; };
  auto ood_elements = l.filter(odd_filter);

  auto odd_it = ood_elements.head_;
  for (int i = 1; i < n; i += 2) {
    REQUIRE(odd_it != nullptr);
    CHECK(i == odd_it->value);
    odd_it = odd_it->next;
  }

  auto gt_than_5 = [](const int &x) { return x > 5; };
  auto gt_than_5_l = l.filter(gt_than_5);

  auto it = gt_than_5_l.head_;
  for (int i = 6; i < n; ++i) {
    REQUIRE(it != nullptr);
    CHECK(i == it->value);
    it = it->next;
  }
}

TEST_CASE("filter_string") {
  generic::list<std::string> l = {"a", "bb", "ccc", "dddd"};
  auto longer_than_2_chars =
      l.filter([](const std::string &s) -> bool { return s.size() > 2; });
  REQUIRE(longer_than_2_chars.size() == 2);
  int num_match = 0;
  for (auto el = longer_than_2_chars.head_; el; el = el->next) {
    if (el->value == "ccc" || el->value == "dddd") {
      ++num_match;
    }
  }
  CHECK(num_match == 2);
}

TEST_CASE("map_string_to_int") {
  generic::list<std::string> l = {"foo", "bar", "baz", "boom"};
  generic::list<int> s_to_i = l.map<int>(
      [](const std::string &s) -> int { return static_cast<int>(s.length()); });
  std::vector<int> expected({3, 3, 3, 4});
  int i = 0;
  for (auto it = s_to_i.head_; it; it = it->next) {
    CHECK(it->value == expected[i++]);
  }
}

TEST_CASE("map_to_upper_case") {
  generic::list<std::string> l = {"foo", "bar", "baz"};
  REQUIRE(l.size() == 3);
  auto upper = l.map<std::string>([](const std::string &s) -> std::string {
    auto copy = s;
    for (char &c : copy) {
      c = std::toupper(c);
    }
    return copy;
  });
  std::vector<std::string> expected = {"FOO", "BAR", "BAZ"};
  int i = 0;
  for (auto it = upper.head_; it; it = it->next) {
    CHECK(it->value == expected[i++]);
  }
}

TEST_CASE("foldLeft_total_chars_of_string_array") {
  generic::list<std::string> l = {"foo", "bar", "baz", "boom"};
  auto len = l.foldLeft<int>(0, [](int cnt, const std::string &el) {
    return cnt + static_cast<int>(el.length());
  });
  CHECK(len == 13);
}

TEST_CASE("foldLeft_concatenate_strings") {
  generic::list<std::string> l = {"foo", "bar", "baz"};
  auto concatenated = l.foldLeft<std::string>(
      "", [](std::string conc, const std::string &el) { return conc + el; });
  CHECK(concatenated == "foobarbaz");
}

TEST_CASE("clear") {
  tracker::reset();
  generic::list<tracker> l;
  for (int i = 0; i < 10; ++i) {
    l.push_back(tracker(i));
  }
  l.clear();
  CHECK(l.size() == 0);
  CHECK(tracker::cnt == 0);
  CHECK(l.head_ == nullptr);
  CHECK(l.tail_ == nullptr);
}

TEST_CASE("copy_from") {
  generic::list<tracker> l;
  int n = 10;
  for (int i = 0; i < n; ++i) {
    l.push_back(tracker(i));
  }
  REQUIRE(tracker::cnt == n);

  generic::list<tracker> l_copy_cons(l);
  REQUIRE(l_copy_cons.size() == n);
  auto el = l_copy_cons.head_;
  for (int i = 0; i < n; ++i) {
    REQUIRE(el != nullptr);
    CHECK(el->value.value == i);
    el = el->next;
  }
  REQUIRE(tracker::cnt == (n * 2));

  generic::list<tracker> l_copy_asgn;
  l_copy_asgn.push_back(tracker(1));
  REQUIRE(l_copy_asgn.size() == 1);
  l_copy_asgn = l;
  el = l_copy_cons.head_;
  for (int i = 0; i < n; ++i) {
    REQUIRE(el != nullptr);
    CHECK(el->value.value == i);
    el = el->next;
  }
  REQUIRE(tracker::cnt == (n * 3));
}

int main(int argc, char *argv[]) {
  Catch::Session session; // There must be exactly once instance

  // writing to session.configData() here sets defaults
  // this is the preferred way to set them

  int returnCode = session.applyCommandLine(argc, argv);
  if (returnCode != 0) // Indicates a command line error
    return returnCode;

  // writing to session.configData() or session.Config() here
  // overrides command line args
  // only do this if you know you need to

  return session.run();
}
