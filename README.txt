
1. General info

OS: ubuntu 16.04 LTS
COMPILER: g++ 5.3.1 with c++11
MAKE: GNU Make 4.1
TESTING: catch 1.5.0 : https://github.com/philsquared/Catch/releases/tag/V1.5.0
FORMATTING: clang-format 3.8 default style
LEAK_CHECKS: valgrind 3.11.0

2. Run and test
make && ./main

to clean: make clean

3. Implementation details

generic_list.h

* I used one header file for the implementation because of heavy template usage to support generic data types.
* stl-like approach when implementing the data structure.
* all the functions are implemented in O(n) time complexity.
* the implementations are NOT THREAT-safe. locks should be used when calling any function; alternative would
    have been to implement a lock-free list but i wasn't sure i'd have a correct implementation in one day;
    basically i had to implement a lock-free way to iterate the list and then all those functions would have
    mapped nicely on that implementation.

4. Sample run

~$ make clean && make && ./main
rm -rf main *.o ~*
g++ -Wall -std=c++11 main.cc -o main
===============================================================================
All tests passed (172 assertions in 19 test cases)
