CXXFLAGS= -Wall -std=c++11

main: main.cc
	g++ $(CXXFLAGS) main.cc -o main

clean:
	rm -rf main *.o ~*
