#pragma once

#include <functional>
#include <initializer_list>

namespace generic {

template <typename T> struct node {
  node(const T &value, node *prev, node *next)
      : value(value), prev(prev), next(next) {}
  node(const T &value) : node(value, nullptr, nullptr) {}

  T value;
  node<T> *prev;
  node<T> *next;
};

// n - length of the list
template <typename T> class list {
public:
  list();
  ~list();

  list(std::initializer_list<T> l);

  list(const list<T> &oth);
  list<T> &operator=(const list<T> &oth) {
    if (&oth == this) {
      return *this;
    }
    list<T>::~list();
    copy_from(oth);
    return *this;
  }

  void reverse(); // O(n) in-place

  list<T> filter(std::function<bool(const T &)> f); // O(n)

  template <typename U> list<U> map(std::function<U(const T &)> f); // O(n)

  template <typename U>
  U foldLeft(U init, std::function<U(U, const T &el)> f); // O(n)

  void push_back(const T &value); // O(1) throws if not enough memory
  void pop_back();                // O(1) undefined behavior on empty list

  int size();   // O(1)
  void clear(); // O(1)

  // private:
  void copy_from(const list &oth); // O(length(oth))

  node<T> *head_;
  node<T> *tail_;
  int size_;
};

template <typename T>
list<T>::list() : head_(nullptr), tail_(nullptr), size_(0) {}

template <typename T> list<T>::~list() {
  auto i = head_;
  while (i) {
    auto to_delete = i;
    i = i->next;
    delete to_delete;
  }
  head_ = nullptr;
  tail_ = nullptr;
  size_ = 0;
}

template <typename T> list<T>::list(std::initializer_list<T> l) {
  head_ = nullptr;
  tail_ = nullptr;
  size_ = 0;
  for (auto &el : l) {
    push_back(el);
  }
}

template <typename T> list<T>::list(const list<T> &oth) {
  head_ = tail_ = nullptr;
  size_ = 0;
  copy_from(oth);
}

template <typename T> void list<T>::reverse() {
  if (size_ == 0) {
    return;
  }
  node<T> *prev = head_;
  node<T> *next = nullptr;
  if (head_) {
    next = head_->next;
  }
  int steps = 0;
  while (next) {
    auto new_next = next->next;
    prev->prev = next;
    next->next = prev;
    prev = next;
    next = new_next;
  }
  std::swap(head_, tail_);
  head_->prev = nullptr;
  tail_->next = nullptr;
}

template <typename T>
list<T> list<T>::filter(std::function<bool(const T &)> f) {
  generic::list<T> l;
  for (auto el = head_; el != nullptr; el = el->next) {
    if (f(el->value)) {
      l.push_back(el->value);
    }
  }
  return l;
}

template <typename T>
template <typename U>
list<U> list<T>::map(std::function<U(const T &)> f) {
  generic::list<U> l;
  for (auto el = head_; el != nullptr; el = el->next) {
    U transformed = f(el->value);
    l.push_back(transformed);
  }
  return l;
}

template <typename T>
template <typename U>
U list<T>::foldLeft(U init, std::function<U(U, const T &)> f) {
  for (auto it = head_; it; it = it->next) {
    init = f(init, it->value);
  }
  return init;
}

template <typename T> void list<T>::push_back(const T &value) {
  auto n =
      new node<T>(value); // will thow if allocation fails, desired behaviour
  if (!tail_) {
    head_ = n;
    tail_ = n;
  } else {
    tail_->next = n;
    n->prev = tail_;
    tail_ = n;
  }
  ++size_;
}

template <typename T> void list<T>::pop_back() {
  auto old_tail = tail_;
  if (size_ == 1) {
    head_ = nullptr;
    tail_ = nullptr;
  } else {
    tail_->prev->next = nullptr;
    tail_ = tail_->prev;
  }
  delete old_tail;
  --size_;
}

template <typename T> int list<T>::size() { return size_; }

template <typename T> void list<T>::clear() { list<T>::~list(); }

template <typename T> void list<T>::copy_from(const list<T> &oth) {
  for (auto el = oth.head_; el != nullptr; el = el->next) {
    push_back(el->value);
  }
  size_ = oth.size_;
}

} // namespace generic
